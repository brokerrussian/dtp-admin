const $log = require('../libs/log');
const $Stats = require('../modules/stats');

module.exports = function (app) {
  app.post('/stats', (req, res) => {
    $Stats.get()
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        $log.error(err);
        res.end('Error');
      });
  });
};